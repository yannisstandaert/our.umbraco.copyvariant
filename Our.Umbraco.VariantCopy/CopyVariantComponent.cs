﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Composing;
using UmbracoWeb = Umbraco.Web;
using Umbraco.Web;
using Umbraco.Web.Trees;
using Umbraco.Core;
using System.Web.UI.WebControls;
using Umbraco.Core.Services;

namespace Our.Umbraco.VariantCopy
{
    public class CopyVariantComponent : IComponent
    {
        private readonly ILocalizedTextService textService;
        public CopyVariantComponent(ILocalizedTextService textService)
        {
            this.textService = textService;
        }
        public void Initialize()
        {
            TreeControllerBase.MenuRendering += TreeControllerBase_MenuRendering;
        }

        private void TreeControllerBase_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            if (int.TryParse(e.NodeId, out int nodeId))
            {
                var node = UmbracoWeb.Composing.Current.UmbracoContext.Content.GetById(nodeId);
                if (node?.ContentType.VariesByCulture() ?? false)
                {                   
                    e.Menu.Items.Add<VariantCopyAction>(textService, opensDialog: true);

                    e.Menu.Items.FirstOrDefault(x => x.Alias.Equals("variantCopyAction")).AdditionalData.Add("actionView", "/app_plugins/VariantCopy/views/copy.html");
                    e.Menu.Items.FirstOrDefault(x => x.Alias.Equals("variantCopyAction")).Name = "Duplicate Variant content";
                }
            }
           

        }

        public void Terminate()
        {
            throw new NotImplementedException();
        }
    }
}
