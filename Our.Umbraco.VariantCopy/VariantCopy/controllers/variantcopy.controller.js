﻿angular.module('umbraco').controller("Umbraco.Editors.Content.VariantCopy", contentVariantCopyController);
function contentVariantCopyController($scope, $routeParams, $http, contentResource) {
    $scope.WelcomeMessage = 'Hello World!';

    var vm = this;
    vm.buttonState = "init";

    vm.clickButton = clickButton;

    function clickButton() {

        vm.buttonState = "busy";
        console.log($routeParams, $scope.currentNode.id);
        //myService.clickButton().then(function () {
        //    vm.buttonState = "success";
        //}, function () {
        //    vm.buttonState = "error";
        //});
        $.ajax({
            type: "GET",
            url: "/umbraco/backoffice/api/copyvariant/CopyToVariants/" + $scope.currentNode.id,
            success: function () {
                vm.buttonState = "success";
            },
            error: function () {
                vm.buttonState = "error";
            },
        });

    }

}