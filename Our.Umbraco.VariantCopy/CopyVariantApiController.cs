﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Core.Services.Implement;
using Umbraco.Web.Composing;
using Umbraco.Web.WebApi;

namespace Our.Umbraco.VariantCopy
{
    public class CopyVariantController : UmbracoAuthorizedApiController
    {
        private readonly IContentService contentService;
        private readonly ILocalizationService localizationService;

        public CopyVariantController(IContentService contentService, ILocalizationService localizationService)
        {
            this.contentService = contentService;
            this.localizationService = localizationService;
        }
        [Route("CopyToVariants/{id}"), HttpGet]
        public IHttpActionResult CopyToVariants([FromUri]int id)
        {
            var languages = localizationService.GetAllLanguages();
            var node = contentService.GetById(id);
            var defaultCultureName = languages.FirstOrDefault(x => x.IsDefault).CultureInfo.ToString();

            foreach (var language in languages)
            {
                if (language.IsDefault)
                {
                    continue;
                }
                var cultureName = language.CultureInfo.ToString();

                if (string.IsNullOrEmpty(node.GetCultureName(cultureName)))
                {
                    node.SetCultureName(node.GetCultureName(defaultCultureName), cultureName);
                }

                foreach (var property in node.Properties.Where(x => x.GetValue(cultureName) == null))
                {
                    if (property.PropertyType.VariesByCulture())
                    {
                        var newValue = property.GetValue(defaultCultureName);
                        node.SetValue(property.Alias, newValue, cultureName);
                    }
                    
                }

            }
            contentService.Save(node);

            return Ok();
        }
    }
}
