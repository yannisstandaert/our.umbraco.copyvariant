﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web.Actions;
using Umbraco.Core;

namespace Our.Umbraco.VariantCopy
{
    class VariantCopyAction : IAction
    {
        public char Letter => 'V';

        public bool ShowInNotifier => true;

        public bool CanBePermissionAssigned => true;

        public string Icon => "documents";

        public string Alias => "variantCopyAction";

        public string Category => "StructureCategories";
    }
}
