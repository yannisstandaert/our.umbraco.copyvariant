﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace Our.Umbraco.VariantCopy
{
    public class CopyVariantComposer : IComposer
    {
        public void Compose(Composition composition)
        {
            composition.Components().Append<CopyVariantComponent>();
        }
    }
}
