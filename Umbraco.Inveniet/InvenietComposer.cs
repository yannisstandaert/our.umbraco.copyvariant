﻿using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Web;

namespace Umbraco.Inveniet
{
    class InvenietComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Sections().Append<InvenietSection>();
            composition.Components().Append<IndexComponent>();
        }
    }
}
