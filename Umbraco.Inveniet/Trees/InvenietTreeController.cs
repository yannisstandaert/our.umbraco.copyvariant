﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;
using Umbraco.Core.Migrations.Upgrade.Common;
using Umbraco.Web.Actions;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Trees;
using Umbraco.Web.WebApi.Filters;

namespace Umbraco.Inveniet.Trees
{
    [Tree("inveniet", "invenietTree", TreeTitle = "Inveniet indexes", TreeGroup = "Indexes", SortOrder = 5)]
    public class InvenietTreeController : TreeController
    {
        protected override TreeNodeCollection GetTreeNodes(string id, [ModelBinder(typeof(HttpQueryStringModelBinder))] FormDataCollection queryStrings)
        {
            Dictionary<int, string> indexes = new Dictionary<int, string>();
            indexes.Add(1, "Indexes");
            var nodes = new TreeNodeCollection();
            foreach (var index in indexes)
            {
                var node = CreateTreeNode(index.Key.ToString(), "-1", queryStrings, index.Value, "icon-presentation", false);
                nodes.Add(node);
            }
            return nodes;
        }
        
        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            // create a Menu Item Collection to return so people can interact with the nodes in your tree
            var menu = new MenuItemCollection();

            if (id == "1")
            {
                // root actions, perhaps users can create new items in this tree, or perhaps it's not a content tree, it might be a read only tree, or each node item might represent something entirely different...
                // add your menu item actions or custom ActionMenuItems
                menu.Items.Add(new CreateChildEntity(Services.TextService));
                // add refresh menu item (note no dialog)
                menu.Items.Add(new RefreshNode(Services.TextService, true));
                return menu;
            }
            // add a delete action to each individual item
            else
            {
                menu.Items.Add<ActionDelete>(Services.TextService, true, opensDialog: true);
            }
            return menu;
        }

        protected override TreeNode CreateRootNode(FormDataCollection queryStrings)
        {
            var root = base.CreateRootNode(queryStrings);

            // set the icon
            root.Icon = "icon-hearts";
            // could be set to false for a custom tree with a single node.
            root.HasChildren = true;
            //url for menu
            root.MenuUrl = null;

            return root;
        }
    }
}
