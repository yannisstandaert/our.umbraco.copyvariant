﻿using Examine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Umbraco.Inveniet
{
    public class IndexComponent : Umbraco.Core.Composing.IComponent
    {
        private readonly IExamineManager _examineManager;

        public IndexComponent(IExamineManager examineManager)
        {
            this._examineManager = examineManager;
        }

        public void Initialize()
        {
            // throw new NotImplementedException();
        }

        public void Terminate()
        {
            // throw new NotImplementedException();
        }
    }
}
