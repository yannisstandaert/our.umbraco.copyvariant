﻿using HtmlAgilityPack;
using ImageProcessor.Processors;
using Our.Umbraco.ProductionCheck.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Composing;
using Umbraco.Web.HealthCheck;


namespace Our.Umbraco.ProductionCheck
{
    [HealthCheck("3c7d6d9f-d1ce-47b5-8cd8-536adceaf0b0", "Metadata",
  Description = "Checks for most essential metadata in html",
  Group = "Production checks")]
    public class MetadataCheck : HealthCheck
    {
        private readonly ILocalizationService localizationService;
        private readonly IEnumerable<ILanguage> languages;
        public MetadataCheck(ILocalizedTextService textService, ILocalizationService localizationService)
        {
            this.localizationService = localizationService;
            this.languages = localizationService.GetAllLanguages();
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            if (HttpContext.Current == null)
            {
                return new List<HealthCheckStatus>();
            }

            var links = GetLocalizedLinks();
            var result = new List<HealthCheckStatus>();
            var link = links.FirstOrDefault().Value;

            using (WebClient client = new WebClient())
            {
                string html = null;
                try
                {
                    html = client.DownloadString(link);
                }
                catch (Exception)
                {
                    var item = new HealthCheckStatus($"couldn't check metadata due to error on page: <strong><a href=\"{link}\">{link}</a></strong>")
                    {
                        ResultType = StatusResultType.Error
                    };
                    result.Add(item);
                    return result;
                }

                var doc = new HtmlDocument();
                doc.LoadHtml(html);

                var metadataService = new MetadataService(html, languages);

                metadataService.CheckForDuplicateMetaTags(ref result);
                metadataService.CheckRobotsMetaTag(ref result);
                metadataService.CheckAuthorMetaTag(ref result);
                metadataService.CheckDescriptionMetaTag(ref result);
                metadataService.CheckTitleMetaTag(ref result);


                
            }

            foreach (var language in languages)
            {
                link = links[language.CultureInfo.ToString()];
                if (link == null)
                {
                    var item = new HealthCheckStatus($"no working link found for language: {language.CultureInfo.ToString()}")
                    {
                        ResultType = StatusResultType.Warning
                    };
                    result.Add(item);
                    return result;
                }
                var currentLanguage = language.CultureInfo.TwoLetterISOLanguageName.ToLower();
                using (WebClient client = new WebClient())
                {
                    string html = null;
                    try
                    {
                        html = client.DownloadString(link);
                    }
                    catch (Exception)
                    {
                        var item = new HealthCheckStatus($"couldn't check metadata due to error on page: <strong><a href=\"{link}\">{link}</a></strong>")
                        {
                            ResultType = StatusResultType.Error
                        };
                        result.Add(item);
                        return result;
                    }

                    MetadataService.CheckHtmlLangAttribute(ref result, link, currentLanguage, html);
                }
            }

            return result;
        }

        private Dictionary<string, string> GetLocalizedLinks()
        {
            var rootNodes = Current.UmbracoContext.Content.GetAtRoot();
            var result = languages.Select(lang => new KeyValuePair<string, string>(lang.CultureInfo.ToString(), null)).ToDictionary(x => x.Key, x => x.Value);
            var counter = 0;
            while (result.Any(x => x.Value == null) && counter < 100)
            {
                ++counter;              
                foreach (var language in languages)
                {
                   
                    var currentCulture = language.CultureInfo.ToString();
                    if (result[currentCulture] != null)
                        continue;
                    
                    var currentUrl = rootNodes.FirstOrDefault(x => !x.Url(currentCulture).Equals("#"))?.Url(currentCulture, mode: UrlMode.Absolute);
                    result[currentCulture] = currentUrl ?? null;
                    var newList = rootNodes.ToList();
                    foreach (var rootNode in rootNodes)
                    {
                        newList.AddRange(rootNode.Children);
                    }
                    rootNodes = newList;
                }
                
            }

            return result;
        } 
    }
}
