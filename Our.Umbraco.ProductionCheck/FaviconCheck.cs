﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Web.HealthCheck;

namespace Our.Umbraco.ProductionCheck
{
    [HealthCheck("b8f44a6c-ddde-474e-873c-27c276e1fded", "favicon.ico",
   Description = "Add a favicon.ico for bookmarks and better SEO",
   Group = "Production checks")]
    public class FaviconCheck : HealthCheck
    {
        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            if (HttpContext.Current == null)
            {
                return new List<HealthCheckStatus>();
            }
            var statuses = new List<HealthCheckStatus>();
            if (!File.Exists(HttpContext.Current.Server.MapPath("~/favicon.ico")))
            {
                statuses.Add(
                new HealthCheckStatus("no favicon.ico found")
                {
                    ResultType = StatusResultType.Error,
                    Actions = new List<HealthCheckAction>()
                });
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath("~/favicon-32x32.png")))
            {
                statuses.Add(
                new HealthCheckStatus("no favicon-32x32.png found")
                {
                    ResultType = StatusResultType.Error,
                    Actions = new List<HealthCheckAction>()
                });
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath("~/favicon-96x96.png")))
            {
                statuses.Add(
                new HealthCheckStatus("no favicon-96x96.png found")
                {
                    ResultType = StatusResultType.Error,
                    Actions = new List<HealthCheckAction>()
                });
            }
            if (!File.Exists(HttpContext.Current.Server.MapPath("~/favicon-16x16.png")))
            {
                statuses.Add(
                new HealthCheckStatus("no favicon-16x16.png found")
                {
                    ResultType = StatusResultType.Error,
                    Actions = new List<HealthCheckAction>()
                });
            }
            if (!statuses.Any())
            {
                statuses.Add(
                new HealthCheckStatus("most essential favicons found")
                {
                    ResultType = StatusResultType.Success,
                    Actions = new List<HealthCheckAction>()
                });
            }
            return statuses;
        }
    }
}
