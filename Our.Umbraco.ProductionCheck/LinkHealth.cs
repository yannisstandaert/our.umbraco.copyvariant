﻿using Superpower.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using Umbraco.Core;
//using Umbraco.Core.Composing;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Composing;
using Umbraco.Web.HealthCheck;
using UmbracoCore = Umbraco.Core;

namespace Our.Umbraco.ProductionCheck
{
    [HealthCheck("ac131fad-c8e2-4c12-8d3e-c55283f0161b", "Link health", Description = "Checks for urls not returning status 200", Group = "Production checks")]
    public class LinkHealth : HealthCheck
    {
        private readonly ILocalizedTextService textService;
        private readonly ILocalizationService localizationService;
        private readonly IEnumerable<ILanguage> languages;
        public LinkHealth(ILocalizedTextService textService, ILocalizationService localizationService) 
        {
            this.textService = textService;
            this.localizationService = localizationService;
            this.languages = localizationService.GetAllLanguages();
        }
        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            if (HttpContext.Current == null)
            {
                return new List<HealthCheckStatus>();
            }
            var rootNodes = Current.UmbracoContext.Content.GetAtRoot();
            var brokenLinks = new List<HealthCheckStatus>();
            var healthyCounter = 0;
            var brokenCounter = 0;
            foreach (var node in rootNodes)
            {
                TestLinks(node, ref brokenLinks, ref healthyCounter, ref brokenCounter);
            }
            brokenLinks.Insert(0, new HealthCheckStatus($"<strong><span class=\"color-green\">{healthyCounter}</span> healthy links</strong><br/><strong><span class=\"color-red\">{brokenCounter}</span> broken or missing links</strong>") { ResultType = StatusResultType.Info});
            return brokenLinks;
        }

        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            throw new NotImplementedException();
        }

        private List<HealthCheckStatus> TestLinks(IPublishedContent node, ref List<HealthCheckStatus> brokenLinks, ref int healthyCounter, ref int brokenCounter)
        {
            foreach (var child in node.ChildrenForAllCultures)
            {
                TestLinks(child, ref brokenLinks, ref healthyCounter, ref brokenCounter);
            }
            foreach (var language in languages)
            {
                var url = node.Url(culture:  language.CultureInfo.ToString(), mode: UrlMode.Absolute);
                if (string.IsNullOrWhiteSpace(url) || url.Equals("#"))
                {
                    var healthStatus = new HealthCheckStatus($"<a href=\"/umbraco#/content/content/edit/{ node.Id }?mculture={ language.CultureInfo }\">{node.Id}</a><br/> &nbsp;&nbsp;&nbsp;&nbsp;<strong>&#8594; <span class=\"color-orange\">404</span></strong>")
                    {
                        ResultType = StatusResultType.Warning
                    };
                    brokenLinks.Add(healthStatus);
                    ++brokenCounter;
                    continue;
                }
                var request = (HttpWebRequest)WebRequest.CreateHttp(url);
                HttpWebResponse response = null;
                request.Method = "HEAD";
                string statusCode = null;
                
                try
                {
                    response = (HttpWebResponse)request.GetResponse();
                    statusCode = response.StatusCode.ToString();
                }
                catch (Exception)
                {
                    statusCode = response == null ? HttpStatusCode.InternalServerError.ToString() : response.StatusCode.ToString();
                }
                if (response?.StatusCode != HttpStatusCode.OK)
                {
                    var healthStatus = new HealthCheckStatus($"<a href=\"/umbraco#/content/content/edit/{ node.Id }?mculture={ language.CultureInfo }\">{node.Id}</a><br/><a href=\"{ url }\">{ url }</a><br/>&nbsp;&nbsp;&nbsp;&nbsp;<strong>&#8594; <span class=\"color-red\">{statusCode}</span></strong>")
                    {
                        ResultType = StatusResultType.Error
                    };
                    brokenLinks.Add(healthStatus);
                    ++brokenCounter;
                    continue;
                }

                ++healthyCounter;

            }

            return brokenLinks;
            
        }
    }
}
