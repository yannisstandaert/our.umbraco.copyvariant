﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Hosting;
using Umbraco.Core.Composing;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using Umbraco.Web.HealthCheck;

namespace Our.Umbraco.ProductionCheck
{
    [HealthCheck("8e8da48f-8cd1-4f53-bd49-04af5ddaf8f3", "robots.txt",
    Description = "Create a robots.txt file to block access to system folders.",
    Group = "Production checks")]
    public class RobotsCheck : HealthCheck
    {
        private readonly ILocalizedTextService _textService;

        public RobotsCheck(ILocalizedTextService textService)
        {
            _textService = textService;
        }

        public override IEnumerable<HealthCheckStatus> GetStatus()
        {
            if (HttpContext.Current == null)
            {
                return new List<HealthCheckStatus>();
            }
            return new[] { CheckForSiteMapInRobotsTxtFile() };
        }

        private HealthCheckStatus CheckForSiteMapInRobotsTxtFile()
        {

            var actions = new List<HealthCheckAction>();
            if (!File.Exists(HttpContext.Current.Server.MapPath("~/robots.txt")))
            {
                actions.Add(new HealthCheckAction("addDefaultRobotsTxtFile", Id) { Name = "Add default robots.txt file"});
                return
                new HealthCheckStatus("no robots.txt found")
                {
                    ResultType = StatusResultType.Error,
                    Actions = actions
                };
            }
            var sitemapLines = File.ReadAllLines(HttpContext.Current.Server.MapPath("~/robots.txt"))
                .Where(line => line.StartsWith("Sitemap:"));
            if (!sitemapLines.Any())
            {
                return
                new HealthCheckStatus("no sitemap(s) defined in robots.txt")
                {
                    ResultType = StatusResultType.Error,
                    Actions = actions
                };
            }

            foreach (var line in sitemapLines)
            {
                var sitemapUrl = line.ToLower().Split(new []{ "sitemap:" }, StringSplitOptions.RemoveEmptyEntries)?.FirstOrDefault()?.Trim();
                try
                {
                    var request = (HttpWebRequest)WebRequest.CreateHttp(sitemapUrl);
                    var response = (HttpWebResponse)request.GetResponse();
                    if (response == null || response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception();
                    }
                }
                catch (Exception e)
                {

                    return new HealthCheckStatus($"invalid sitemap(s) defined in robots.txt: <strong><a href=\"{sitemapUrl}\">{sitemapUrl}</a></strong>")
                    {
                        ResultType = StatusResultType.Error,
                        Actions = actions
                    };
                }
            }
            return
                new HealthCheckStatus("sitemap urls seem fine")
                {
                    ResultType = StatusResultType.Success,
                    Actions = actions
                };

        }


        public override HealthCheckStatus ExecuteAction(HealthCheckAction action)
        {
            switch (action.Alias)
            {
                case "addDefaultRobotsTxtFile":
                    return AddDefaultRobotsTxtFile();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

       

        private HealthCheckStatus AddDefaultRobotsTxtFile()
        {
            var success = false;
            var message = string.Empty;
            const string content = @"# robots.txt for Umbraco
User-agent: *
Disallow: /umbraco/
Disallow: /umbraco
Disallow: /dist/";


            try
            {
                File.WriteAllText(HostingEnvironment.MapPath("~/robots.txt"), content);
                success = true;
            }
            catch (Exception exception)
            {
                Current.Logger.Error(this.GetType(), exception, "Could not write robots.txt to the root of the site");
            }

            return
                new HealthCheckStatus(message)
                {
                    ResultType = success ? StatusResultType.Success : StatusResultType.Error,
                    Actions = new List<HealthCheckAction>()
                };
        }
    }
}
