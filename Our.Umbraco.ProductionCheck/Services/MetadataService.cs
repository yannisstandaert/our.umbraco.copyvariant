﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web.HealthCheck;

namespace Our.Umbraco.ProductionCheck.Services
{
    public class MetadataService
    {
        public string Html { get; set; }
        private readonly HtmlDocument doc;
        private readonly HtmlNodeCollection metaTags;
        private readonly IEnumerable<ILanguage> languages;
        public MetadataService(string html, IEnumerable<ILanguage> languages)
        {
            this.Html = html;
            this.languages = languages;
            doc = new HtmlDocument();
            doc.LoadHtml(html);
            metaTags = doc.DocumentNode.SelectNodes("//meta");
        }

        public void CheckForDuplicateMetaTags(ref List<HealthCheckStatus> results)
        {
            foreach (var metaTag in metaTags)
            {
                if (metaTags.Any(x => x.GetAttributeValue("name", string.Empty).Equals(metaTag.GetAttributeValue("name", string.Empty), StringComparison.InvariantCultureIgnoreCase) && metaTag != x))
                {
                    var item = new HealthCheckStatus($"meta tag <strong>{metaTag.GetAttributeValue("name", string.Empty)}</strong> was found multiple times")
                    {
                        ResultType = StatusResultType.Error
                    };
                    if (!results.Any(x => item.Message.Equals(x.Message)))
                    {
                        results.Add(item);
                    }
                }
            }
        }

        public void CheckRobotsMetaTag(ref List<HealthCheckStatus> results)
        {
            if (metaTags.Any(x => x.GetAttributeValue("name", string.Empty).Equals("robots", StringComparison.InvariantCultureIgnoreCase)))
            {
                var robotsTag = metaTags.FirstOrDefault(x => x.GetAttributeValue("name", string.Empty).Equals("robots", StringComparison.InvariantCultureIgnoreCase));

                if ((robotsTag.GetAttributeValue("content", string.Empty).ToLower().Contains("noindex") || robotsTag.GetAttributeValue("content", string.Empty).ToLower().Contains("nofollow")))
                {
                    var item = new HealthCheckStatus("meta tag <strong>robots</strong> is set to <i>noindex</i> and/or <i>nofollow</i>")
                    {
                        ResultType = StatusResultType.Info
                    };
                    results.Add(item);
                }
                else if ((robotsTag.GetAttributeValue("content", string.Empty).ToLower().Contains("index") || robotsTag.GetAttributeValue("content", string.Empty).ToLower().Contains("follow")))
                {
                    var item = new HealthCheckStatus("meta tag <strong>robots</strong> is set to <i>index</i> and/or <i>follow</i>")
                    {
                        ResultType = StatusResultType.Info,
                    };
                    results.Add(item);
                }
                else
                {
                    var item = new HealthCheckStatus("meta tag <strong>robots</strong> has no (valid) content")
                    {
                        ResultType = StatusResultType.Error,
                    };
                    results.Add(item);
                }

            }
            else
            {
                var item = new HealthCheckStatus("meta tag <strong>robots</strong> is missing")
                {
                    ResultType = StatusResultType.Error
                };
                results.Add(item);
            }
        }

        public void CheckAuthorMetaTag(ref List<HealthCheckStatus> results)
        {
            if (metaTags.Any(x => x.GetAttributeValue("name", string.Empty).Equals("author", StringComparison.InvariantCultureIgnoreCase)))
            {
                var authorTag = metaTags.FirstOrDefault(x => x.GetAttributeValue("name", string.Empty).Equals("author", StringComparison.InvariantCultureIgnoreCase));
                if ((authorTag.GetAttributeValue("content", string.Empty) != string.Empty))
                {
                    var item = new HealthCheckStatus($"meta tag <strong>author</strong> is set to <i>{authorTag.GetAttributeValue("content", string.Empty)}</i>")
                    {
                        ResultType = StatusResultType.Info
                    };
                    results.Add(item);
                }
                else
                {
                    var item = new HealthCheckStatus("meta tag <strong>author</strong> has no content")
                    {
                        ResultType = StatusResultType.Error
                    };
                    results.Add(item);
                }

            }
            else
            {
                var item = new HealthCheckStatus("meta tag <strong>author</strong> is missing")
                {
                    ResultType = StatusResultType.Error
                };
                results.Add(item);
            }
        }

        public void CheckDescriptionMetaTag(ref List<HealthCheckStatus> results)
        {
            if (metaTags.Any(x => x.GetAttributeValue("name", string.Empty).Equals("description", StringComparison.InvariantCultureIgnoreCase)))
            {
                var descriptionTag = metaTags.FirstOrDefault(x => x.GetAttributeValue("name", string.Empty).Equals("description", StringComparison.InvariantCultureIgnoreCase));
                if ((descriptionTag.GetAttributeValue("content", string.Empty) != string.Empty))
                {
                    var item = new HealthCheckStatus($"meta tag <strong>description</strong> is set to <i>{descriptionTag.GetAttributeValue("content", string.Empty)}</i>")
                    {
                        ResultType = StatusResultType.Info
                    };
                    results.Add(item);
                }
                else
                {
                    var item = new HealthCheckStatus("meta tag <strong>description</strong> has no content")
                    {
                        ResultType = StatusResultType.Error
                    };
                    results.Add(item);
                }

            }
            else
            {
                var item = new HealthCheckStatus("meta tag <strong>description</strong> is missing")
                {
                    ResultType = StatusResultType.Error
                };
                results.Add(item);
            }
        }

        public void CheckTitleMetaTag(ref List<HealthCheckStatus> results)
        {
            if (metaTags.Any(x => x.GetAttributeValue("name", string.Empty).Equals("title", StringComparison.InvariantCultureIgnoreCase)))
            {
                var titleTag = metaTags.FirstOrDefault(x => x.GetAttributeValue("name", string.Empty).Equals("title", StringComparison.InvariantCultureIgnoreCase));
                if ((titleTag.GetAttributeValue("content", string.Empty) != string.Empty))
                {
                    var item = new HealthCheckStatus($"meta tag <strong>title</strong> is set to <i>{titleTag.GetAttributeValue("content", string.Empty)}</i>")
                    {
                        ResultType = StatusResultType.Info
                    };
                    results.Add(item);
                }
                else
                {
                    var item = new HealthCheckStatus("meta tag <strong>title</strong> has no content")
                    {
                        ResultType = StatusResultType.Error
                    };
                    results.Add(item);
                }

            }
            else
            {
                var item = new HealthCheckStatus("meta tag <strong>title</strong> is missing")
                {
                    ResultType = StatusResultType.Error
                };
                results.Add(item);
            }
        }

        public static void CheckHtmlLangAttribute(ref List<HealthCheckStatus> results, string url, string language, string html)
        {
            var langDoc = new HtmlDocument();
            langDoc.LoadHtml(html);
            var htmlTag = langDoc.DocumentNode.ChildNodes.FirstOrDefault(x => x.Name.Equals("html", StringComparison.InvariantCultureIgnoreCase));
            var langPropertyValue = htmlTag.GetAttributeValue("lang", string.Empty);
            if (string.IsNullOrEmpty(langPropertyValue))
            {
                var item = new HealthCheckStatus("html <strong>lang</strong> attribute is missing")
                {
                    ResultType = StatusResultType.Error
                };
                results.Add(item);
            }
            else if (!langPropertyValue.Trim().Equals(language, StringComparison.InvariantCultureIgnoreCase))
            {
                var item = new HealthCheckStatus($"<a href=\"{url}\">{url}</a><br/>expected <i>{language}</i> for html <strong>lang=\"\"</strong> attribute, found <i>{langPropertyValue.Trim().ToLower()}</i> instead ")
                {
                    ResultType = StatusResultType.Error
                };
                results.Add(item);
            }
        }

    }
}
